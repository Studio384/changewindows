<p class="meta build-date">
	<span class="release-version">
		08.04.'14
	</span>
</p>
<h2 class="ctr-info">Windows 8.1 "Blue" Update &middot; 9600</h2>
<a href="img/build/9600u.png"><img src="img/build/9600u.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Start and All apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now shut down Windows from the start screen (not for devices with touchscreen or smaller than 8,5 inch)</li>
	<li>You can now search from the start screen with a visual button</li>
	<li>A right-click on a tile now shows a context menu</li>
	<li>The default start screen has been updated for new users</li>
	<li>The start screen now shows a notification when a new app has been installed</li>
	<li>You can now show more apps in the All apps view</li>
	<li>All apps now shows the letter of each category always</li>
</ul>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>It's now possible to pin Windows Apps</li>
	<li>The Windows Store is now a default pinned app</li>
	<li>Apps now have a titlebar</li>
	<li>The taskbar now can be accessed within apps</li>
	<li>Devices without a touchscreen now boot to desktop by default</li>
	<li>Devices without a touchscreen now have desktop apps set as default</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>&quot;Enterprise mode&quot; has been added to Internet Explorer</li>
	<li>There are new options in the F12-tools</li>
	<li>Improved support for ECMAScript 5.1</li>
	<li>Improved support for WebGL</li>
</ul>
<h3>Build-in apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>OneDrive
		<ul>
			<li>SkyDrive is now OneDrive</li>
			<li>It's now possible to synchronise manually</li>
			<li>There is now a OneDrive icon in the system tray</li>
		</ul>
	</li>
	<li>Xbox Music
		<ul>
			<li>You can now pin artists and playlists to your start screen</li>
			<li>You can now use drag and drop</li>
			<li>Xbox Music will now show Backwards, Forwards and Play/Pause under its Aero Peak window on the taskbar</li>
		</ul>
	</li>
	<li>PC Settings
		<ul>
			<li>You can now manage your disk space</li>
			<li>You can now give your PC a new name from the app</li>
			<li>You can now leave a domain from the app</li>
			<li>A link to the Control Panel hass been added</li>
		</ul>
	</li>
</ul>
<h3>And futher<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now show more details in the Networks fly-out for a network</li>
	<li>Improved performance and support for WIMBoot</li>
</ul>