<p class="meta build-date">
	<span class="release-version">
		18.09.'15
  </span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold R2" &middot; 10547 &middot; Developing</h2>
<?php echo $alerts; ?>
<a href="img/build/10547.png"><img src="img/build/10547.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now show either 3 or 4 medium tiles per group
	  <ul>
	    <li>When enabled, tiles won't become bigger in start screen and tablet mode</li>
      </ul>
	</li>
	<li>The letters in the app list now take less space when &quot;Show more tiles&quot; is enabled</li>
	<li>Tiles now can have jumplists</li>
	<li>Cortana now also works for local accounts</li>
	<li>The context menus for tiles have been reorganized
	  <ul>
	    <li>You can now share apps trough this menu</li>
	    <li>You can now review apps trough this menu</li>
      </ul>
	</li>
	<li>Desktop app tiles now can have their own color</li>
</ul>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Context menus have now have a grey design instead of white</li>
	<li>The look for dropdowns has been changed system wide</li>
	<li>When you snap a window and select another window to snap beside it, you can resize both windows at once (only horizontal)
	  <ul>
	    <li>This needs to be enabled under System &gt; Multitasking</li>
      </ul>
	</li>
</ul>
<h3>Tablet mode<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Task View now allows to snap apps to the screen or replace apps</li>
</ul>
<h3>Lock screen<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Windows Spotlight can now be set as the lock screen for Pro users</li>
	<li>You can now disable the background for the logon screen</li>
</ul>
<h3>Microsoft Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Edge has been upgraded from version 20.10532 to 21.10547</li>
	<li>EdgeHTML has been upgraded from version 12.10532 to 13.10547</li>
	<li>Extended support for HTML5 and CSS3
		<ul>
			<li>Support for the download-attribute in an anchor</li>
			<li>Support for the picture-element</li>
			<li>Support for :in-range, :out-of-range, :read-only and :read-write</li>
			<li>Support for font-size-adjust</li>
			<li>Support for input type=datetime-local</li>
			<li>Support for ellipses</li>
			<li>Support for Object RTC</li>
			<li>Support for HTML Templates</li>
		</ul>
	</li>
	<li>You can now open proxy settings from within the Microsoft Edge settings</li>
	<li>Edge has a new First start-home page</li>
	<li>You can now share media to other screens</li>
	<li>When a page is properly paginated, the next button will light up to go to the next page</li>
	<li>Tabs won't resize when you're closing tabs until your mouse leaves the tabbar</li>
</ul>
<h3>Settings<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now (dis)allow apps to access your call history</li>
	<li>You can now (dis)allow apps to access and send emails</li>
	<li>You can now make Windows select the correct time zone automatically</li>
	<li>The &quot;Storage&quot; tab now has icons for each file format</li>
	<li>You can let Windows notify you when there are issues with any USB-connected device</li>
	<li>The Insider settings now show your account details</li>
</ul>
<h3>Windows Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Windows Apps now have an opening and closing animation</li>
	<li>Windows Apps now can have jumplists</li>
	<li>Many apps have been updated</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
  <li>Improvements to the text input panel, including expanding when more space is required and punctuation improvements</li>
  <li>Some icons have been replaced</li>
	<li>There is a new system in place to warn you for common issues when using the Insider Preview</li>
</ul>
