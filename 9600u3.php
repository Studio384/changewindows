<p class="meta build-date">
	<span class="release-version">
		15.09.'15
	</span>
</p>
<h2 class="ctr-info">Windows 8.1 "Blue" Update 3 &middot; 9600</h2>
<p>Windows 8.1 Update 3 is an update that is only available to Windows RT-devices. Regular Windows 8.1 editions do not receive this update.</p>
<a href="img/build/9600u3.png"><img src="img/build/9600u3.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h2>What's new in Windows 8.1 Update 2 build 9600.18007?</h2>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The start menu has been added with support for Live Tiles</li>
	<li>The &quot;Jumplists&quot; tab under &quot;Taskbar and Start Menu Properties&quot; has been renamed to &quot;Start Menu&quot;</li>
	<li>An option to switch to the start menu has been added under &quot;Start Menu&quot; in &quot;Taskbar and Start Menu Properties&quot;</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Avatars on the logon screen and start menu are now circles</li>
</ul>