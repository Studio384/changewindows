<p class="meta build-date">
	<span class="release-version">
		29.06.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10158</h2>
<?php echo $alerts; ?>
<a href="img/build/10158.png"><img src="img/build/10158.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>More apps now use the accent color as their tile color</li>
	<li>The full screen start menu now shows the power and all apps button at the bottom</li>
	<li>The hamburger button no longer shows a notification icon when new apps are installed, this is now shown at the all apps button</li>
	<li>New animations for tiles, including the added-then-removed-now-added-again 3D-animation</li>
	<li>New boot animations for apps</li>
	<li>Most WinRT tiles can now take the large tile size (including Edge, Calculator, etc)</li>
	<li>The smilies for additional icons above power have been replaced with actual indicating icons (and some have been updated)</li>
	<li>Full screen start is now aligned higher, allowing more tiles in the height without a scrollbar appearing</li>
	<li>Search will now show a &quot;Try Cortana&quot; button on the bottom if Cortana isn't activated</li>
	<li>You can now swipe up on the left side of start to show all apps</li>
</ul>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The window chrome of Win32 apps is now the same as for WinRT apps</li>
	<li>The clock, language and other notification-fly-outs have been updated with a transparent background</li>
	<li>The back-button no longer takes the accent color in some apps</li>
	<li>Apps no longer have to take a white icon on the taskbar anymore</li>
	<li>More apps have a tile-less icon on the taskbar</li>
	<li>Multiple icons have been revamped</li>
	<li>All Win32 controls have been given a redesign</li>
	<li>The taskbar animations for loading, needs attention, etc. have been updated</li>
	<li>The installation experience has been updated, it also introduces more apps</li>
	<li>There are new default wallpapers</li>
	<li>Propertie windows have received a visual update</li>
	<li>In tablet mode, icons no longer disappear behind the Task View, Search and Back button when app icons are shown</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Action center
		<ul>
			<li>New quick settings for Notes and Quiet hours</li>
		</ul>
	</li>
	<li>Cortana
		<ul>
			<li>Cortana now has a refined UI</li>
			<li>Additional features have been added, like flight tracking, etc.</li>
			<li>Cortana now supports Office 365 integration</li>
			<li>Cortana will sometimes &quot;talk&quot; to you through the searchbox</li>
		</ul>
	</li>
	<li>Windows Spotlight
		<ul>
			<li>The Windows Spotlight lockscreen has been removed</li>
			<li>Start will no longer show suggested apps</li>
		</ul>
	</li>
	<li>You can now set a timer in the snapping tool</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Project Spartan has been updated from version 15 to 20 and is now called Microsoft Edge</li>
	<li>Edge has new icons</li>
	<li>The feedback icon has been removed from the main bar</li>
	<li>There is a new share icon on the url bar</li>
	<li>You can now enable a dark theme for Edge</li>
	<li>You can now drag and drop tabs between multiple windows</li>
	<li>Improved ECMAScript 6 support (without flags)</li>
	<li>You can now manage your passwords</li>
	<li>You can now change the search provider</li>
	<li>When closing Edge with 2 or more tabs open, it will now warn you</li>
	<li>Pinned hubs now look better</li>
	<li>You can now show a home button</li>
	<li>Pinning sites to start is broken in that it no longer has the site's theme, but a normal Edge logo</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Multiple apps have been updated to more recent versions (that already where available for earlier builds)
		<ul>
			<li>Most stock apps no longer have a background color for their icon in the taskbar</li>
		</ul>
	</li>
	<li>Store
		<ul>
			<li>The &quot;Store&quot; apps has been removed, leaving only &quot;Store (beta)&quot;</li>
			<li>The Store has received a new design with an interface more similar to other apps (back button in titlebar, etc.)</li>
		</ul>
	</li>
	<li>New &quot;Get Office&quot; app</li>
	<li>New &quot;Phone companion&quot; app</li>
	<li>New &quot;Microsoft Wi-Fi&quot; app</li>
	<li>Photos
		<ul>
			<li>The photos app now supports GIFs</li>
			<li>You can now choose to open a picture with another app</li>
		</ul>
	</li>
	<li>Insider Hub
		<ul>
			<li>The Insider Hub has to be activated in &quot;Add features&quot;</li>
			<li>There Insider Hub has a new icon</li>
		</ul>
	</li>
	<li>Settings
		<ul>
			<li>There are new animations when switching applets</li>
			<li>Automatic color picker now works </li>
			<li>You can now enable features that aren't enabled by default trough Windows Update</li>
			<li>You can now exit the Windows Insider program</li>
		</ul>
	</li>
</ul>