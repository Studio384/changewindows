<p class="meta build-date">
	<span class="release-version">
		10.04.'15
	</span>
</p>
<h2 class="ctr-info">Windows Phone 8.1 "Blue" Update 2 &middot; 9651</h2>
<?php echo $alerts; ?>
<h2>What's new in Windows Phone 8.1 build 15116.125?</h2>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>De "All apps" arrow has a new design and now shows a text label</li>
</ul>
<h3>Settings<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Settings is now devided into multiple categories</li>
	<li>You can now search through settings</li>
	<li>You can now pin settings to the start screen</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Bangla, Khmer, Kiswahili and Lao are now supported languages</li>
	<li>You can now restore the start screen to its original state</li>
	<li>Support for the MKV container</li>
</ul>
<h3>System technologies<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Support for Voice over LTE</li>
	<li>Support for Bluetooth Messaging Access Profile</li>
	<li>Support for Bluetooth Human Interface Device Profile 1.1</li>
	<li>Support for Bluetooth HID over Generic Attribute Profile</li>
	<li>Support for Absolute volume Control</li>
	<li>Support for VPN Configuration Service Provider</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 15127.138?</h2>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Anti-theft protection added</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 15143.154?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved support for updating to Windows 10 Mobile</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 15148.160?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved support for updating to Windows 10 Mobile</li>
</ul>