<p class="meta build-date">
	<span class="release-version">
		27.08.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold R2" &middot; 10532</h2>
<?php echo $alerts; ?>
<a href="img/build/10532.png"><img src="img/build/10532.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Context menus have now a more consistent design</li>
	<li>Improved dark and light theming</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved support for ECMAScript 6.0 and 7.0</li>
	<li>Support for new input types</li>
	<li>Support for Pointer Lock</li>
	<li>Support for Canvas blending modes</li>
	<li>Additional improved support for standards</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now share feedback more easly</li>
</ul>