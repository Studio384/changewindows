<p class="meta build-date">
	<span class="release-version">
		18.08.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold R2" &middot; 10525</h2>
<?php echo $alerts; ?>
<a href="img/build/10525.png"><img src="img/build/10525.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>New color options</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Base for Object RTC support</li>
</ul>
<h3>Settings<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Settings-icon on the taskbar has no longer a background color</li>
	<li>The Settings-tile is now transparent for non-fresh installs too</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved memory management</li>
</ul>