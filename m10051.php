<p class="meta build-date">
	<span class="release-version">
		10.04.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 10051</h2>
<?php echo $alerts; ?>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Large and High tile sizes are no longer available</li>
	<li>App Switcher
		<ul>
			<li>The app switcher has a new design</li>
			<li>It will now show 4 apps per page on large devices</li>
			<li>The orientation has been shifted to left to right</li>
		</ul>
	</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Keyboard
		<ul>
			<li>The keyboard can be made smaller for one-handed usage</li>
		</ul>
	</li>
	<li>Support for notifications trough LEDs</li>
	<li>Support for pen and inking</li>
	<li>More fileds now support speech input</li>
	<li>You can now disconnect your Microsoft Account</li>
	<li>You can now set app permissions for location</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Battery Saver
		<ul>
			<li>You can now set a limit from 5% up to 60% for batteyr saver</li>
		</ul>
	</li>
	<li>New Maps app</li>
	<li>Xbox Music now supports OneDrive syncing</li>
	<li>Outlook Mail and Outlook Calendar have been added, additionaly to Mail and Calendar</li>
	<li>New People app</li>
	<li>New Messaging app</li>
	<li>New Phone app</li>
	<li>Project Spartan is now avaialble as a new browser</li>
	<li>Office Mobile has been removed</li>
</ul>