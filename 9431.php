<p class="meta build-date">
	<span class="release-version">
		26.06.'13
	</span>
</p>
<h2 class="ctr-killed">Windows 8.1 "Blue" &middot; 9431</h2>
<?php echo $alerts; ?>
<a href="img/build/9431.png"><img src="img/build/9431.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Tiles<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Small and Large tiles have been added to the start screen.</li>
	<li>You can now change multiple tiles at once.</li>
	<li>Tiles can now show their download status on the startscreen and All apps list.</li>
	<li>Tiles for desktop apps now get an accent color based on their most prominent color.</li>
	<li>Improved support for portret mode.</li>
	<li>Browser now always use their Modern UI-version of their tile, even if it isn't the default browser.</li>
	<li>When pinning a tile to the startscreen from within an app, you can now choose its size.</li>
</ul>
<h3>All Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There is an &quot;All apps&quot; button on the bottom of the screen when using a mouse and keyboard.</li>
	<li>You can now sort apps on name, date, most used and category.</li>
	<li>There is now a search field.</li>
	<li>You can now access All apps by swyping up from the start screen.</li>
	<li>New apps aren't pinned to the start screen by default anymore.</li>
	<li>You can now change the behavior of search when searching in the All apps screne.</li>
	<li>You can now display desktop apps first.</li>
	<li>New apps are now labeled as &quot;New&quot;.</li>
</ul>
<h3>Personalize<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Changing the start screen will now start a new edit mode.</li>
	<li>You can now set the desktop background as the start screen background.</li>
	<li>There are multiple new tatoos added to the startscreen (including dynamic tatoos).</li>
	<li>Multiple new colors have been added to choose from.</li>
	<li>The startscreen can now show up to 9 rows of tiles.</li>
</ul>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>When Snap is being used, Windows will ask where to open an app.</li>
	<li>Apps now open in snap view when triggered from within another app.</li>
	<li>Apps now can be selected by holding them.</li>
	<li>Snap can now use multiple sizes.</li>
	<li>You can now snap up to 4 apps per screen (depeding on your resolution).</li>
	<li>You can now use multiple Windows Apps on multiple screens.</li>
	<li>The start button has now a visual image again.</li>
	<li>You can now choose to boot to the desktop.</li>
	<li>You can now disable hot corners.</li>
	<li>If you have a high resolution, the charmsbar will (depending on the used hotcorner) appear higher or lower.</li>
	<li>You can now add an app to alarm notifications.</li>
	<li>The keyboard has been improved with numerous features.</li>
	<li>You can now set a presentation as lock screen.</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Tabs can now be synced.</li>
	<li>Click-to-call is a new feature.</li>
	<li>You can now choose to always display the address bar and tabs.</li>
	<li>You can now open more then 10 tabs.</li>
	<li>A Download list has been added.</li>
	<li>Tiles can now have more formats.</li>
	<li>Reading View is a new option that opens a reading-optimized environment.</li>
	<li>Website Tiles can now show a feed.</li>
	<li>Improved support for CSS3 and HTML5.</li>
	<li>Support for WebGL, MDEG Dash and SPDY.</li>
	<li>Brand now F12-Tools.</li>
</ul>
<h3>Build-in apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>All apps now support portrait mode.</li>
	<li>You can now remove apps from more devices at once.</li>
	<li>Camera
		<ul>
			<li>The Camera-app now has a new color and interface.</li>
			<li>The app now users Microsoft Photosynth technology.</li>
			<li>You can now access the camera from the lock screen.</li>
		</ul>
	</li>
	<li>Finance
		<ul>
			<li>There is a search feature.</li>
		</ul>
	</li>
	<li>News
		<ul>
			<li>News now has a build-in search feature.</li>
		</ul>
	</li>
	<li>PC Settings
		<ul>
			<li>Your start screen can now sync between devices.</li>
			<li>Multiple new features have been added.</li>
			<li>The app has a major revamped interface.</li>
			<li>You can now pin PC Settings to the start screen.</li>
		</ul>
	</li>
	<li>Photos
		<ul>
			<li>There are multiple new options to change photos.</li>
			<li>Facebook- and Flickr-integration has been removed.</li>
		</ul>
	</li>
	<li>SkyDrive
		<ul>
			<li>It's now possible to view local files from the app.</li>
			<li>You can now choose to save files to SkyDrive.</li>
			<li>SkyDrive has been integrated better into the OS.</li>
		</ul>
	</li>
	<li>Sport
		<ul>
			<li>Sport has now a build-in search feature.</li>
		</ul>
	</li>
	<li>Travel
		<ul>
			<li>Travel now has a build-in search feature.</li>
		</ul>
	</li>
	<li>Weather
		<ul>
			<li>Weather now has a build-in search feature.</li>
			<li>When started for the first time, Weather will show a &quot;Get started&quot;-window.</li>
		</ul>
	</li>
	<li>Windows Store
		<ul>
			<li>The Windows Store a has received a major revamp.</li>
			<li>Apps will now be updated automatically.</li>
			<li>The icon has been changed.</li>
			<li>The Windows Store tile now shows spotlight apps.</li>
		</ul>
	</li>
	<li>New apps
		<ul>
			<li>Skype - replaces Messaging</li>
			<li>Calculator</li>
			<li>Bing Drink and Food</li>
			<li>Bing Health and Fitness</li>
			<li>Help &amp; Tips</li>
			<li>Alarms</li>
			<li>Reading List</li>
			<li>Search</li>
		</ul>
	</li>
	<li>Removed apps
		<ul>
			<li>Messaging - replaced by Skype</li>
			<li>Bing - replaced by Search</li>
		</ul>
	</li>
</ul>
<h3>And futher<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Support for NFC, 3G and 4G.</li>
	<li>Wireless printing without additional drivers.</li>
	<li>2-step authentication.</li>
	<li>DirectX 11.2 is now available.</li>
	<li>The Windows Prestation-index has been removed.</li>
	<li>The Windows kernel has been updated from version 6.2 to 6.3.</li>
	<li>Native support for 3D-printers.</li>
	<li>New installation interface.</li>
	<li>You can now replace Command Prompt with PowerShelll.</li>
	<li>Improved support for high resolutions.</li>
</ul>