<p class="meta build-date">
	<span class="release-version">
		15.07.'15
	</span>
</p>
<h2 class="ctr-success">Windows 10 "Threshold" &middot; 10240</h2>
<?php echo $alerts; ?>
<a href="img/build/10240.png"><img src="img/build/10240.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Task view has been added</li>
	<li>Suppport for multiple desktops</li>
	<li>Task view replaced the App Switcher</li>
	<li>Windows Apps apps now can run on the desktop</li>
	<li>Aero Snap has been updated
		<ul>
			<li>You can now snap up to 4 windows</li>
			<li>When you snap a window and resize it, then snap another window to the other side, it will fill up the left screen instead of half the screen</li>
			<li>When snapping an app, Windows will prompt a overview of other running apps to snap along with Snap Assist</li>
		</ul>
	</li>
	<li>Windows Apps now can create a shortcut on the desktop or any ohter folder</li>
	<li>You can now pin the Recycle Bin to the taskbar</li>
	<li>You can now scroll in windows by pointing the cursor to it</li>
	<li>When used on a touch enabled device, the icons in the notification area have more space</li>
	<li>Windows has a new iconset</li>
	<li>You can now choose to display all active apps from all desktops in the taskbar, or only the apps that are on the current desktop</li>
	<li>The Action Center icon has been added to the taskbar</li>
	<li>There is now a search bar and Task View button on the taskbar by default</li>
	<li>Multiple animations have been altered</li>
	<li>There are new default wallpapers</li>
</ul>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The start menu has been added with support for Live Tiles</li>
	<li>Continuum is now available</li>
	<li>The Start grid has been improved</li>
	<li>The Start screen grid now scrolls vertically</li>
	<li>Tiles no longer use a gradient and border</li>
	<li>It is now possible to resize the start menu</li>
	<li>It's now possible for Windows Apps to have a tile color that matches the accent color</li>
	<li>Tiles for Win32 apps no longer take a color based on the icon of that tile, but follows the accent color</li>
	<li>The start menu now can be made transparent</li>
	<li>Support for jumplists</li>
	<li>You can now click on a letter to show all letters and navigate faster</li>
	<li>You can now pin tiles by draging apps from the &quot;All apps&quot; list to the start screen</li>
	<li>You can now put the PC to sleep with the power button in start</li>
	<li>Folders now have an arrow to indicate that they contain a dropdown</li>
	<li>Recently installed apps are now displayed on the top of the all apps list</li>
	<li>Tiles can now be changed (format, etc) with touch, similar to Windows Phone</li>
</ul>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Windows interface has been redesigned</li>
	<li>Dialogs in WinRT are now windowed</li>
	<li>The Charms bar has been removed</li>
	<li>The start button now has a smaller Windows logo and a new hover design</li>
	<li>When clicking on the network icon, a new window shows up to manage your connection</li>
	<li>The battery status popup has been updated</li>
	<li>The login screen has been redesigned</li>
	<li>Loading screens for Restarting, Shutting down, etc. have been redesigned</li>
	<li>The taskbar is now dark by default</li>
	<li>You can now enable and disable the color of the taskbar and startscreen</li>
	<li>There is a new calendar fly-out for the system tray</li>
	<li>The notification center now takes on the color you've choosen for windows and the taskbar</li>
	<li>Windows no longer take on a color and are now always gray</li>
	<li>Propertie windows have received a visual update</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Action Center
		<ul>
			<li>The Action Center has been added</li>
			<li>You can now change quick settings in the Action Center</li>
			<li>Notifications now can contain actions</li>
			<li>A swipe from the right now opens the Action Center</li>
			<li>Quick settings can now contain more settings</li>
			<li>Notifications have received a new design</li>
		</ul>
	</li>
	<li>Cortana has been added</li>
	<li>Windows + C now opens Cortana instead of the Charms bar</li>
	<li>OneDrive now uses selective sync, no more Smart Files</li>
	<li>The virtual keyboard now shows AutoComplete suggestions</li>
	<li>It's easier to connect to wireless audio and video devices</li>
	<li>ALT+TAB now opens a view that's merged with the Task View</li>
	<li>Classic Calculator has been removed</li>
	<li>Backup &amp; restore has been readded from Windows 7</li>
	<li>&quot;Play to&quot; has been renamed &quot;Cast to Device&quot;</li>
	<li>Command prompt
		<ul>
			<li>Standarized key shortcuts</li>
			<li>Selecting text is now possible</li>
			<li>Past from clipboard is now possible</li>
			<li>Text wrap can be altered on resize</li>
			<li>Leading zeros can now be trimmed in selections</li>
			<li>You can alter the opacity of the window between 30 and 100 procent</li>
		</ul>
	</li>
	<li>Touch gestures on a touchpad are now build in and universal across all Windows devices</li>
	<li>Improvements to multi monitor support have been made</li>
	<li>&quot;System compression&quot; has been added to Disk Cleanup as a new option</li>
	<li>You can now choose the default folder for File Explorer</li>
	<li>You can now print to PDF natively</li>
	<li>Windows Hello
		<ul>
			<li>Support for loging in with face recognition</li>
			<li>Support for loging in with fingerprints</li>
			<li>Support for loging in with eye scan</li>
			<li>"Passport" will replace passwords</li>
		</ul>
	</li>
	<li>Win32 apps can no longer ask to change the default app for any file format and have to use the methode used by Windows Apps</li>
	<li>You can now install apps from and move apps to an SD card</li>
	<li>Context menus now have a touch-friendly design when using a touch screen (and a redesign in general)</li>
	<li>The Text Input canvas has been improved</li>
</ul>
<h3>File Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Home is the new default location</li>
	<li>An &quot;Add to favorites&quot; button has been added under &quot;Start&quot; in the ribbon</li>
	<li>An &quot;Share&quot; button has been added under &quot;Share&quot; in the ribbon
		<ul>
			<li>You can now share files from within the File Explorer with compatible Windows Apps</li>
		</ul>
	</li>
</ul>
<h3>Microsoft Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Microsoft Edge is the new default browser, replacing Internet Explorer</li>
	<li>You can now draw on web-pages</li>
	<li>Comments can now be added to web-pages (like Office)</li>
	<li>Reading Mode is now build-in for desktops</li>
	<li>Reading List is now build-in</li>
	<li>Cortana is now build-in</li>
	<li>Support for HTML5, CSS3 and Javascript (ECMAScript 2015) has been improved</li>
	<li>Support for HTTP/2 has been added</li>
	<li>You can now find your downloads in the download manager</li>
	<li>You can now open Internet Explorer from within Edge</li>
	<li>The &quot;Add to&quot; dialogue has been improved</li>
	<li>You can now save PDFs you opened in the browser</li>
	<li>Improved performance for the Chakra Javascript-engine</li>
	<li>Public Suffix List is now used for interoperable Top Level Domain parsing</li>
	<li>Experimental Features Dashboard has been added under about:flags</li>
	<li>There is a new Internet Explorer-like New tabs page</li>
	<li>There is a new MSN.com-like New tabs page</li>
	<li>Pages that are using sound now show this in their tab</li>
	<li>The User Agent String has been altered</li>
	<li>F12 developer tools
		<ul>
			<li>New and improved Network Tools</li>
			<li>HTML and CSS Pretty Printing support</li>
			<li>Async Callstacks for Events and Timers</li>
			<li>Sourcemaps for Styles and in the Memory Profiler</li>
			<li>Find references and Go To Definitions</li>
			<li>And many other changes</li>
		</ul>
	</li>
</ul>
<h3>Touch<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Touching a textbox in the desktop will now auto start the onscreen keyboard</li>
	<li>Swiping down from the screens edge will show an apps titlebar</li>
	<li>Win32 apps now also use gestures</li>
	<li>When tablet mode is enabled, the Windows, Search/Cortana and Task view icons get bigger (the actual icons, not just the space arround them)</li>
	<li>The taskbar is now always visible by default</li>
</ul>
<h3>Technologies<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Build-in mkv-file support</li>
	<li>Build-in hvec-file support</li>
	<li>System-wide support for the FLAC & ALAC audio formats</li>
</ul>
<h3>Installation<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The installation and OOBE UI have been updated</li>
	<li>The installation now shows a percentage that is a total of the full installation, rather then a percentage for each step</li>
	<li>Cortana, Edge, Groove and other apps now gets introduced during installation</li>
	<li>It's now more clear how to make a new Microsoft Account, or how to use a local account</li>
</ul>
<h3>Build-in apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<p>We will no longer cover changes to build-in apps other then Microsoft Edge as this is almost impossible to keep track off and updates to these apps aren't bound to any version of Windows. And in case you do want to know the differences between the build-in Windows 8.1 and build-in Windows 10 apps: they are basically all redone.</p>
<h3>And futher<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Windows kernel major version number changed from version 6.3 to 10.0</li>
	<li>IE is no longer a default pinned item on the taskbar and is replaced by "Project Spartan"'s globe icon</li>
	<li>Support for the Persian calendar</li>
</ul>