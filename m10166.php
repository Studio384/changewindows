<p class="meta build-date">
	<span class="release-version">
		10.07.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 10166</h2>
<?php echo $alerts; ?>
<h3>General<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Edge and Store now use the accent color for their tiles</li>
	<li>The battery icon is now larger</li>
</ul>
<h3>Security<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now reset your PIN after 5 failed logins through your Microsoft Account</li>
	<li>After failing to many attempts on a PIN, you'll now get a prompt to type "A1B2C3" instead of being locked out for a given time</li>
</ul>
<h3>Store<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Windows Phone Store is no longer available</li>
	<li>The Windows Store is no longer in beta</li>
</ul>