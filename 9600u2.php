<p class="meta build-date">
	<span class="release-version">
		21.10.'14
	</span>
</p>
<h2 class="ctr-info">Windows 8.1 "Blue" Update 2 &middot; 9600</h2>
<p>After the Windows 8.1 Update release of April 2014, many sites and Microsoft-watchers concidered the update releasedi n August 2014 &quot;Windows 8.1 Update 2&quot;. Microsoft explained that Update 2 didn't exist as a whole update, but is spread out over multiple months. In particular May, June and August, as these months contained some significant changes to the OS. To stay as complete as possible, we've decided to call the updates from these 3 months together &quot;Windows 8.1 Update 2&quot;. This way, the Windows 8.1 Update 3-update in September 2015 makes more sense, too.</p>
<a href="img/build/9600u.png"><img src="img/build/9600u.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h2>What's new in Windows 8.1 Update 2 build 9600.17107?</h2>
<h3>Store<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The navigation bar is now always visible</li>
	<li>Hitlists and Collections have been added</li>
	<li>The start page has received a small redesign</li>
	<li>The navigation for mouse and keyboard users has been improved</li>
	<li>Apps now show if they are an Universel app</li>
	<li>Promotions are now shown on an app's page when available</li>
</ul>
<h2>What's new in Windows 8.1 Update 2 build 9600.17126?</h2>
<h3>OneDrive<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There is a new fly-out in the system tray</li>
	<li>You can now force OneDrive to sync</li>
	<li>You can now pause a sync</li>
	<li>The fly-out now shows when was the last time OneDrive synced</li>
	<li>There is a new option to open the OneDrive-folder in the Explorer</li>
	<li>The dropdownmenu in the system tray has been extended with more option</li>
	<li>There is now a link to the OneDrive-site</li>
	<li>You can now manage your OneDrive-storage through the system tray</li>
	<li>A link to OneDrive settings has been added</li>
	<li>Multiple help items have been added</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved support for vertical writing</li>
</ul>
<h2>What's new in Windows 8.1 Update 2 build 9600.17249?</h2>
<h3>PC Settings<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Windows Update now show when it last checked on updates and when it last installed updates</li>
	<li>You can now choose to enable or disable the touchpad when a mouse is attached</li>
	<li>It's now possible to enable right clicks on a mousepad</li>
	<li>Idem dito for double clicks</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved support for WebGL</li>
	<li>New updates to the F12-tools</li>
	<li>Start of WebDriver-support</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Support for Miracast Receive</li>
	<li>You'll now see less dialogues when using SharePoint Online</li>
	<li>The Ruble-icon is now supported</li>
	<li>Improved WiFi Direct APIs</li>
	<li>New API for reading the gps-data and date recorded of an MP4-file</li>
</ul>