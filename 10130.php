<p class="meta build-date">
	<span class="release-version">
		29.05.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10130</h2>
<?php echo $alerts; ?>
<a href="img/build/10130.png"><img src="img/build/10130.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>More apps now use the accent color as their tile color (Photos, Alarms &amp; Clocks, Maps, etc)</li>
	<li>You can add new locations to the left side part of the menu</li>
	<li>When new apps are available, there will be a little badge on the &quot;All apps&quot; button and hamburger menu</li>
</ul>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Some of the icons have been revamped</li>
	<li>For virtual desktops, the taskbar will now be filtered by default</li>
	<li>There is now an universal back button on the taskbar in tablet mode</li>
	<li>The progress bar behind icons in the taskbar now works for every state again and has been redesigned</li>
	<li>The clock widget in the taskbar has been redesigned</li>
	<li>Jumplists have been redesigned</li>
	<li>The Action Center no longer has a border</li>
	<li>You can now swipe down from the edge of a screen to show app comands like Windows 8.1</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Cortana
		<ul>
			<li>Windows + C now opens Cortana instead of the Charms bar</li>
		</ul>
	</li>
	<li>Printing to PDF is now called &quot;Microsoft print to PDF&quot;</li>
</ul>
<h3>Project Spartan<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Project Spartan has been updated from version 0.13 to 0.15</li>
	<li>Project Spartan is now capable of going full screen, where it used to go in full window</li>
	<li>The Settings pane has been split in 2 pages</li>
	<li>It is now possible to pin panes to the side to let them stay open while browsing</li>
	<li>Improved print options</li>
	<li>Improvements to address bar badges</li>
	<li>Reading Mode now supports more screensizes and content types</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Video</li>
	<ul>
		<li>The full screen feature now works properly</li>
	</ul>
	<li>Solitaire Collection
		<ul>
			<li>Solitaire Collection Preview has been renamed to Solitaire Collection</li>
		</ul>
	</li>
	<li>Network beta
		<ul>
			<li>A &quot;Network beta&quot; app has been added, but doesn't contain anything</li>
		</ul>
	</li>
	<li>MSN apps
	  <ul>
		<li>The stock MSN apps have been updated to a newer version (Weather, News, Sports, Money, etc.)</li>
	  </ul>
	</li>
	<li>Store Beta
	  <ul>
		<li>The stock Store has been updated to a newer version</li>
		<li>The interface has been improved</li>
		<li>You can now see all of your purchases in &quot;My collection&quot;</li>
	  </ul>
	</li>
</ul>