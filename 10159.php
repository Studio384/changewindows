<p class="meta build-date">
	<span class="release-version">
		30.06.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10159</h2>
<?php echo $alerts; ?>
<a href="img/build/10159.png"><img src="img/build/10159.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Hero wallpaper is now available</li>
	<li>The logon screen has been updated with a centered design</li>
	<li>The Hero wallpaper is now used in the logon screen</li>
</ul>
