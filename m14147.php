<p class="meta build-date">
	<span class="release-version">
		04.08.'15
	</span>
</p>
<h2 class="ctr-info">Windows Phone 8.1 "Blue" Update &middot; 14147</h2>
<?php echo $alerts; ?>
<h2>What's new in Windows Phone 8.1 build 14147.180?</h2>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The default start screen has been updated for new devices</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now create folders on the start screen</li>
	<li>Cortana
		<ul>
			<li>China can now choose a different appearance for Cortana</li>
			<li>You can now use Cortana when your phone is locked</li>
		</ul>
	</li>
	<li>Apps Corner is a new mode to use a pre-selected series of apps</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improvements to the rendering of sites</li>
	<li>Internet Explorer has a new User Agent String</li>
	<li>Some Webkit-previxes are now supported</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Alarms
		<ul>
			<li>You can now set a snooze time for an alarm</li>
		</ul>
	</li>
	<li>Messaging
		<ul>
			<li>You can now send sms-messages to other people when already send</li>
			<li>There are now management options for messages</li>
		</ul>
	</li>
	<li>Phone
		<ul>
			<li>You can now remove calls</li>
		</ul>
	</li>
	<li>Settings
		<ul>
			<li>Devices without a SIM now can update the time automaticaly</li>
		</ul>
	</li>
	<li>Store
		<ul>
			<li>Store now has a live tile</li>
		</ul>
	</li>
	<li>Xbox Music
		<ul>
			<li>Music now has its live tile back</li>
			<li>Support for Kids Corner</li>
			<li>Improved performance</li>
		</ul>
	</li>
</ul>
<h3>System technologies<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Support for Voice over LTE</li>
	<li>Support for Netwrok Time Protocol</li>
	<li>You can now share internet over Bluetooth</li>
	<li>CDMA- + GSM-dual-sim support</li>
	<li>VPN over WiFi hotspot support</li>
	<li>Support for smartcovers</li>
	<li>Support for the 800x1280, 540x960 (limited to screens under 6 inch) screen resolutions</li>
	<li>The 768x1280 resolution can now be used for devices up to 7 inch</li>
	<li>Support for Bluetooth PAN 1.0</li>
	<li>Support for Bluetooth aptX-codec and A2DP</li>
	<li>Support for Bluetooth AVRCP</li>
	<li>Support for Qualcomm Qucik charge 2.0 (Snapdragon 400, 600 and 800)
	<li>APIs for linked devices, like smart watches</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14157.200?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved stability</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14176.243?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>HTC Windows Phone 8X, 8S and 8XT are now supported</li>
	<li>Fixes a bug that prevented Windows Phone 8.1 Update from installing</li>
	<li>Fixes a bug in Access Violation</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14192.280?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Test for "Critical updates"</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14192.280?</h2>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Battery Saver
		<ul>
			<li>Battery Saver now has a live tile</li>
		</ul>
	</li>
	<li>Settings
		<ul>
			<li>You can now set an installation time for updates</li>
		</ul>
	</li>
</ul>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>NTP-sync is now enabled by default</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14219.314?</h2>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Cortana
		<ul>
			<li>Cortana is now available in French, German, Italian and Spanich</li>
		</ul>
	</li>
	<li>Windows Phone can use an SD-card to update if there is not enough space on the device</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Settings
		<ul>
			<li>There is now a "Mobile" switch for the Action Center</li>
		</ul>
	</li>
</ul>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now save folder on an SD-card</li>
	<li>Support for hardware keyboards</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14226.359?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Fixes issues with the screen of the Microsoft Lumia 535</li>
</ul>
<h2>What's new in Windows Phone 8.1 build 14234.375?</h2>
<h3>And furhter<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Fixes issues with the screen of the Microsoft Lumia 535</li>
</ul>