<p class="meta build-date">
	<span class="release-version">
		14.05.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 10080</h2>
<?php echo $alerts; ?>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There are multiple new live tile animations</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Keyboard
		<ul>
			<li>You can now change the language of the keyboard by swiping over the space bar</li>
			<li>You can now use the top row to add numbers</li>
		</ul>
	</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Internet Explorer is no longer available</li>
	<li>Full screen support</li>
	<li>InPrivate mode is now available</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>New apps
		<ul>
			<li>Store beta</li>
			<li>Getting started</li>
			<li>Music Preview</li>
			<li>Video Preview</li>
			<li>Xbox App</li>
		</ul>
	</li>
	<li>This version also supports Excel Mobile, Word Mobile and PowerPoint Mobile</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Windows 10 Mobile now supports pointer devices</li>
</ul>