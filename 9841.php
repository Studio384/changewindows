<p class="meta build-date">
	<span class="release-version">
		01.10.'14
	</span>
</p>
<h2 class="ctr-killed">Windows 10 "Threshold" &middot; 9841</h2>
<?php echo $alerts; ?>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Task view has been added</li>
	<li>Suppport for multiple desktops</li>
	<li>Task view replaced the App Switcher</li>
	<li>Modern UI apps now can run on the desktop</li>
	<li>Aero Snap has been updated
		<ul>
			<li>You can now snap up to 4 windows</li>
			<li>When you snap a window and resize it, then snap another window to the other side, it will fill up the left screen instead of half the screen</li>
			<li>When snapping an app, Windows will prompt a overview of other running apps to snap along</li>
		</ul>
	</li>
	<li>Universal Apps now can create a shortcut on the desktop or any ohter folder</li>
	<li>You can now pin the Recycle Bin to the taskbar</li>
	<li>You can now scroll a windows by pointing the cursor to it</li>
	<li>When used on a touch enabled device, the icons in the notification area have more space</li>
</ul>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The start menu has been added with support for Live Tiles</li>
	<li>An option to switch back to the start screen has been added under &quot;Start Menu&quot; in &quot;Taskbar and Start Menu Properties&quot;</li>
	<li>The Charmsbar can't be accessed with the mouse anymore, instead, a new menu button has to be clicked (keyboard and touch still available)</li>
</ul>
<h3>File Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Home is the new default location</li>
	<li>An &quot;Add to favorites&quot; button has been added under &quot;Start&quot; in the ribbon</li>
	<li>An &quot;Share&quot; button has been added under &quot;Share&quot; in the ribbon
		<ul>
			<li>You can now share files from within the File Explorer with compatible Universal Apps</li>
		</ul>
	</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Support for HTTP/2 has been added</li>
	<li>Improved performance for the Chakra Javascript-engine</li>
	<li>The Immersive Internet Explorer has been removed</li>
	<li>Public Suffix List is now used for interoperable Top Level Domain parsing</li>
</ul>
<h3>Build-in apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Default apps have been updated to more recent versions</li>
	<li>PC Settings
	<ul>
		<li>It's now possible to update to new Preview Builds with one click</li>
		<li>You can now add Active Directory accounts</li>
	</ul>
	</li>
	<li>Search
		<ul>
			<li>The Search-app is now a default pinned item on the taskbar and can't be removed</li>
			<li>Search now uses a new interface, similar to Bings search results</li>
		</ul>
	</li>
</ul>
<h3>And futher<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Some icons have been replaced</li>
	<li>The virtual keyboard now shows AutoComplete suggestions</li>
	<li>Command prompt
		<ul>
			<li>Standarized key shortcuts</li>
			<li>Selecting text is now possible</li>
			<li>Past from clipboard is now possible</li>
			<li>Text wrap can be altered on resize</li>
			<li>Leading zeros can now be trimmed in selections</li>
			<li>You can alter the opacity of the window between 30 and 100 procent</li>
		</ul>
	</li>
	<li>Preview Updates can be rolled back in the EUFI screen</li>
</ul>