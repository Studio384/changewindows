<p class="meta build-date">
	<span class="release-version">
		11.09.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold R2" &middot; 10536</h2>
<?php echo $alerts; ?>
<p>Microsoft announced the release of build 10536 for mobile devices but cancelled the update shortly before the full roll-out began due to a bug that made it impossible to upgrade to future builds. The update was already staged and build 10536.1003 was rolled out to 200 Windows Insiders. While this is a very small roll-out, we do concider it a public release and are thus listening September 11, 2015 as this build's release date. This build was re-released as 10536.1004 on September 15, 2015.</p>
<h2>What's new in Windows 10 Mobile build 10514.0?</h2>
<p>When upgrading from 10512, Windows Insiders will also get build 10514 delivered. This build is required to overcome an upgrading issue. Upgrading from Windows Phone 8.1 won't trigger this build.</p>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Unknown changes</li>
</ul>
<h2>What's new in Windows 10 Mobile build 10536.1000?</h2>
<h3>General<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Make device visible for USB Ethernet Emulation Model</li>
	<li>Make device show up trough USB connection and in local network</li>
	<li>One-handed mode now available on all devices</li>
	<li>Support for Japanese and English (India) voice input</li>
</ul>
<h3>Microsoft Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Improved support for HTML5</li>
	<li>Improved support for ECMAScript 6 en 7</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Insider Hub is now included again</li>
	<li>The Photos app now has a folder view</li>
	<li>You can now share your feedback from the Windows Feedback app</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Mobile hotspot is now functional again</li>
	<li>Quiet Hours\Do Not Disturb is also fixed</li>
	<li>Pinch to zoom in maps has been fixed to work as expected</li>
	<li>The delay for the lockscreen (to show time and data and unlock) has been fixed</li>
</ul>
<h2>What's new in Windows 10 Mobile build 10536.1003?</h2>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Unknown changes, contained a bug that disallowed future updates</li>
</ul>
<h2>What's new in Windows 10 Mobile build 10536.1004?</h2>
<h3>Fixes<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Fixes a bug that caused later updates to fail</li>
	<li>Fixes a bug that caused issues when purchasing something trough the store</li>
</ul>