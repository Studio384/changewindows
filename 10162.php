<p class="meta build-date">
	<span class="release-version">
		02.07.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10162</h2>
<?php echo $alerts; ?>
<a href="img/build/10162.png"><img src="img/build/10162.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Bug fixes<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
<li>Improved reliablility, stability and battery usage</li>
</ul>