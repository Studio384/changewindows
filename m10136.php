<p class="meta build-date">
	<span class="release-version">
		16.06.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 10136</h2>
<?php echo $alerts; ?>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There are multiple UI improvements</li>
	<li>Pressing and holding the Windows-button will make it interface come down</li>
	<li>The search button in "All apps" has been replaced with a search box</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Cortana
		<ul>
			<li>Improved Cortana interface</li>
		</ul>
	</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Internet Explorer is no longer available</li>
	<li>Full screen support</li>
	<li>InPrivate mode is now available</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Settings
		<ul>
			<li>You can now set 3G-only has highest connection speed</li>
			<li>You now get to see a splitted interface in landscape mode on larger devices</li>
		</ul>
	</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Both VPN Point to Point Tunnelling Protocol (PPTP) and Secure Socket Tunnelling Protocol (SSTP) are now supported</li>
	<li>Video stabilization</li>
</ul>