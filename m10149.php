<p class="meta build-date">
	<span class="release-version">
		25.06.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 10149</h2>
<?php echo $alerts; ?>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There are new animations</li>
	<li>Multiple icons have been updated</li>
	<li>The "All apps" lists jumplist has received a small update</li>
	<li>Actions should now respond faster</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Action center
		<ul>
			<li>New quick settings for Flashlights</li>
		</ul>
	</li>
	<li>Cortana
		<ul>
			<li>Updated notebook</li>
			<li>You can now set up quiet hours in the notebook</li>
		</ul>
	</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Project Spartan has been updated from version 16 to 19 and is now called Microsoft Edge</li>
	<li>Edge has new icons</li>
	<li>The feedback icon has been removed from the main bar</li>
	<li>There is a new share icon on the url bar</li>
	<li>The url bar is now shown at the bottom</li>
	<li>You can now enable a dark theme for Edge</li>
	<li>Improved ECMAScript 6 support (without flags)</li>
	<li>You can now manage your passwords</li>
	<li>You can now change the search provider</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Multiple apps have been updated, or have updates available in the Store (beta)</li>
	<li>Photos
		<ul>
			<li>Photos now supports GIF-images</li>
		</ul>
	</li>
	<li>OneDrive
		<ul>
			<li>You can now change the Camera roll-sync settings in OneDrive</li>
		</ul>
	</li>
	<li>Insider Hub
		<ul>
			<li>The Insider Hub is temporarly gone</li>
		</ul>
	</li>
</ul>