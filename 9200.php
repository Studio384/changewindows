<p class="meta build-date">
	<span class="release-version">
		26.10.'12
	</span>
</p>
<h2 class="ctr-success">Windows 8 "8" &middot; 9200</h2>
<?php echo $alerts; ?>
<a href="img/build/9200.png"><img src="img/build/9200.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The start menu has been replaced by a full screen start</li>
	<li>The start button is now a hot corner instead of a visible button</li>
	<li>The Charms bar has been added to the right side of the screen for central management</li>
	<li>There is a new lockscreen interface</li>
	<li>The user login screen has been redesigned</li>
	<li>The window chrome has been redesigned and flattened out</li>
	<li>The glass effect has been removed from the taskbar</li>
	<li>There is now an app switcher on the left side of the screen</li>
	<li>Windows Explorer is renamed to File Explorer and has now a ribbon-based interface
		<ul>
			<li>The "up" button from Windows XP is back</li>
			<li>You can now pause an action</li>
			<li>All copy, past, snip and other actions are now shown in one single window</li>
			<li>The "Choose wich file to keep"-dialogue has been redesigned to show more info</li>
			<li>The Preview-panel has been reworked</li>
			<li>You can now drag and drop folders to the breadcrumb path</li>
		</ul>
	</li>
	<li>The taskmanager has been redesigned and shows more information, it also has a minimized view</li>
	<li>The on-screen keyboard has been redesigned</li>
</ul>
<h3>Developers<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The WinRT (Windows Runtime) has been added</li>
	<li>Windows Store has been added as a central place for apps</li>
	<li>There are new notification capabilities for WinRT apps</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Internet Explorer now has a modern app</li>
	<li>Adobe Flash is now integrated into IE</li>
	<li>You can now swipe left and right to go back or forward in your search history</li>
	<li>Many improvements to HTML5, CSS3 and JavaScript support</li>
	<li>Multiple new APIs</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Password fields now show a peek button</li>
	<li>You can now login with a picture password or pin code</li>
	<li>File History can be used to bring back files</li>
	<li>Windows now used a "hybrid boot" to boot up faster</li>
	<li>Windows To Go can be used to cary a Windows installation on an USB-stick</li>
	<li>SmartScreen is now integrated in Windows</li>
	<li>Windows Defender has been replaced by Microsoft Security Essentials (through the brand is still "Windows Defender")</li>
	<li>You can now Refresh and Reset your Windows installation</li>
	<li>Family Safety is now intergrated into Windows</li>
	<li>Hyper-V is now optionaly available to all Windows 8 Pro users</li>
</ul>
<h3>Build-in apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>New apps
		<ul>
			<li>Mail, People, Messages and Calendar has been added</li>
			<li>Photos has been added</li>
			<li>Music has been added</li>
			<li>Video has been added</li>
			<li>Camera has been added</li>
			<li>Reader has been added</li>
			<li>Internet Explorer has been added</li>
			<li>Bing Search, Bing News, Bing Weather, Bing Travel, Bing Sports and Bing Finance have been added</li>
			<li>SkyDrive has been added</li>
			<li>Games has been added</li>
			<li>PC Settings has been added</li>
		</ul>
	</li>
</ul>
<h3>And futher<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Support for USB 3.0 is now included natively</li>
	<li>You can now login with your Microsoft Account</li>
	<li>DirectX 11.1 is included with Windows 8</li>
	<li>Support for SIM cards</li>
	<li>Windows now supports UEFI</li>
	<li>There is a plethora of new keyboard shortcuts</li>
</ul>