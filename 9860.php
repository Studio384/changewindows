<p class="meta build-date">
	<span class="release-version">
		21.10.'14
	</span>
</p>
<h2 class="ctr-killed">Windows 10 "Threshold" &middot; 9860</h2>
<?php echo $alerts; ?>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Action Center icon has been added to the taskbar</li>
	<li>Universal Apps now have a black titlebar instead of a colored one</li>
	<li>Clicking the internet connection icon on the taskbar will open the PC Settings app instead of the fly-out</li>
	<li>Multiple annimations have been altered</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Notifications are now added to the Action Center</li>
	<li>Build-in mkv-file support</li>
	<li>Build-in hvec-file support</li>
	<li>Universal scrolling has been disabled</li>
</ul>
<h3>Touch<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Touching a textbox in the desktop will now auto start the onscreen keyboard</li>
	<li>Swiping down from the screens edge will show an apps titlebar</li>
</ul>
<h3>WinRT<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Apps can now run on a 1024x600 resolution</li>
</ul>
<h3>Build-in apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>PC Settings
		<ul>
			<li>DataSense is added to manage data connections</li>
			<li>Battery Saver is added to manage energy consumption on mobile devices</li>
			<li>You can now choose if you want new Windows builds as soon as they are released, or want to wait a couple of days</li>
		</ul>
	</li>
	<li>zPC Settings has been added as a new default app (changes are compared to PC Settings)
		<ul>
			<li>Multiple sections have been renamed, replaced, merged, or split</li>
			<li>You can now add Optional Features through the app</li>
			<li>Battery Saver (Devices) has been added to show usage details</li>
			<li>Personalization settings have been added</li>
			<li>Closed captioning can now be set in the app</li>
			<li>Multiple privacy settings have been added</li>
			<li>There are applets, through empty, for following items (sorted by section)
				<ul>
					<li>System: Windows Defender, Battery saver (System), Speech, Storage sense</li>
					<li>Network and WiFi: Mobile hotspot, Data Sense, VPN, DirectAccess, Ethernet, Mobile broadband</li>
					<li>Personalization: Themes, Background, Colors, Sounds</li>
					<li>OEM</li>
				</ul>
			</li>
		</ul>
	</li>
</ul>