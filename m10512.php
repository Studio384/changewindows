<p class="meta build-date">
	<span class="release-version">
		12.08.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold R2" &middot; 10512</h2>
<?php echo $alerts; ?>
<h3>General<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now change your lock screen and background from the Photos app</li>
	<li>The tile layout for Kids Corner has been changed</li>
	<li>There are new wallpapers available, dated April 1975</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>This is the first Windows Threshold R2 release</li>
	<li>Bugfixes</li>
</ul>