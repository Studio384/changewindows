<p class="meta build-date">
	<span class="release-version">
		09.07.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10166</h2>
<?php echo $alerts; ?>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>You can now buy WiFi in the Windows Store</li>
	<li>You can now view localhost sites within Microsoft Edge</li>
</ul>
<h3>Bug fixes<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Bug fixes</li>
</ul>