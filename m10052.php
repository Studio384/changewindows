<p class="meta build-date">
	<span class="release-version">
		21.04.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 10052</h2>
<?php echo $alerts; ?>
<h3>General<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Bug fixes</li>
</ul>