<p class="meta build-date">
	<span class="release-version">
		12.02.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 Mobile "Threshold" &middot; 9941</h2>
<?php echo $alerts; ?>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Large and High tile sizes are now supported</li>
	<li>Wallpapers are now shown behind the tiles instead of in them</li>
	<li>New context menu design</li>
	<li>Recent installed apps are now shown at the top of the app list</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Action Center
		<ul>
			<li>Notifications can now be expanded and contain actions</li>
			<li>Quick settings have been extended with more toggles</li>
			<li>Notifications now get synced between devices</li>
			<li>Quick settings now switch on or off their respective setting, pressing them will bring you to the settings app</li>
		</ul>
	</li>
	<li>When the device is turned off, it will remind you of your next appointment</li>
	<li>The keyboard now has a pointing stick</li>
	<li>You can now encrypt your device</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Internet Explorer now uses the Edge render engine</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>File explorer has been addedµ</li>
	<li>Camera has been updated with mutliple new features (similar to Lumia Camera)</li>
	<li>Alarms app has been extended with world clocks, timer and stopwatch functions</li>
	<li>Calculator can now be used to convert units</li>
	<li>Settings has been redesigned, similar to Windows 10 for desktops</li>
	<li>Photos has bee nredesigned, similar to Widnows 10 for desktops</li>
	<li>Sound recorder has been added</li>
</ul>