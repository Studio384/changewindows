<p class="meta build-date">
	<span class="release-version">
		29.04.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10074</h2>
<?php echo $alerts; ?>
<a href="img/build/10074.png"><img src="img/build/10074.png" class="img-responsive build-img" alt="Screenshot current build" /></a>
<h3>Start and search<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Cortana is now integrated into start</li>
	<li>Cortana's height is now variable to start</li>
	<li>The Cortana icon is now smaller to match the start button</li>
	<li>The search field, when disabled, will no longer cover the taskbar when opening the start menu, searching by typing is still possible</li>
	<li>The search field now appears in the start menu when not enabled on the taskbar</li>
	<li>When in fullscreen, the groups are verticaly centered</li>
	<li>You can now make up to 5 columns of groups</li>
	<li>Start can now be set to use a blur</li>
	<li>There are new Tile animations</li>
	<li>Live Tiles can now be disabled</li>
	<li>When tablet mode is enabled, the Windows, Search/Cortana and Task view icons get bigger (the actual icons, not just the space arround them)</li>
</ul>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There is a new sound control</li>
	<li>Some of the sounds have been renamed</li>
	<li>The personalization settings have been removed from the Control Panel (except for themes)</li>
	<li>&quot;Personalize&quot; in the desktop context menu now opens the Settings app</li>
	<li>You can now enable &quot;Battery saver&quot; or change the brightness from the &quot;Battery status&quot; icon in the taskbar</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Getting started
		<ul>
			<li>Gettings started has a new tile icon (which appeared on the splash screen in 10061)</li>
		</ul>
	</li>
	<li>Settings
		<ul>
			<li>The category icon is now shown next to the back-arrow</li>
			<li>Some icons have been changed</li>
			<li>The list of default accent colors has been changed</li>
			<li>Update &amp; back-up has been renamed Update &amp; security</li>
			<li>Windows Defender settings are now also displayed under Update &amp; security</li>
		</ul>
	</li>
	<li>People
		<ul>
			<li>People has been revamped</li>
		</ul>
	</li>
</ul>
<h3>And further<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>This build indicates the first build in the Insider Preview series instead of the Technical Preview</li>
	<li>The installation now says &quot;Installing Windows&quot; rather then &quot;Installing Windows 10 Technical Preview&quot;</li>
</ul>