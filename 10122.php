<p class="meta build-date">
	<span class="release-version">
		20.05.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10122</h2>
<?php echo $alerts; ?>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The "Expand start" button has been removed</li>
	<li>Support for jumplists</li>
	<li>&quot;Places&quot; has been removed</li>
	<li>File Explorer and Settings are now shown above Power options</li>
	<li>Start will now give a suggestion for new apps</li>
	<li>You can now click on a letter to show all letters and navigate faster</li>
	<li>Tablet improvements
		<ul>
			<li>The apps list is no longer displayed onscreen by default</li>
			<li>Tiles are displayed larger</li>
			<li>There is now a global back button</li>
		</ul>
	</li>
	<li>Some tiles (Cortana) will now follow the accent color</li>
</ul>
<h3>Interface<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Apps will now display the back-button in the titlebar when required, tablets will show this button in the taskbar in a later build</li>
	<li>Switch-buttons and sliders have received a new design</li>
	<li>The window border chrome has been updated</li>
	<li>The installation has a slighly altered design (the ring now fades, there are only 3 steps instead of 4)</li>
</ul>
<h3>Features<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Backup &amp; restore has been readded from Windows 7</li>
	<li>&quot;Play to&quot; has been renamed &quot;Cast to Device&quot;</li>
	<li>Cortana
		<ul>
			<li>In China, users can now choose between Cortana and Huna</li>
			<li>Cortana now has a Feedback button</li>
			<li>Cortana now has a dark interface</li>
			<li>Cortana now shows a smaller interface when using the &quot;Hey Cortana&quot; command</li>
		</ul>
	</li>
	<li>Windows Hello
		<ul>
			<li>Support for loging in with face recognition</li>
			<li>Support for loging in with fingerprints</li>
			<li>Support for loging in with eye scan</li>
			<li>"Passport" will replace passwords</li>
		</ul>
	</li>
	<li>Win32 apps can no longer ask to change the default app for any file format and have to use the methode used by Windows Apps</li>
</ul>
<h3>Edge<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>There is a new Internet Explorer-like New tabs page</li>
	<li>There is a new MSN.com-like New tabs page</li>
	<li>Pages that are using sound now show this in their tab</li>
	<li>You can now pin sites to start again</li>
	<li>Edge can now save passwords and forms</li>
	<li>Edge now supports InPrivate browsing</li>
	<li>When Reading Mode-compatible content is found, the icon will animate</li>
	<li>When hovering over an URL, the target URL is now shown at the bottom</li>
	<li>Improvements to the Edge render engine
		<ul>
			<li>CSS3 Cursor Value support</li>
			<li>HTML5 date/week/time input fields are now enabled by default</li>
		</ul>
	</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Music Preview
		<ul>
			<li>Music Preview is now called Music</li>
			<li>Feature and interface update</li>
		</ul>
	</li>
	<li>Video Preview
		<ul>
			<li>Video Preview is now called Video</li>
			<li>Feature and interface updates</li>
		</ul>
	</li>
	<li>Settings
		<ul>
			<li>Settings has a new interface design</li>
			<li>Some applets have been moved to other sections</li>
			<li>Updates &amp; Security &gt; For Developers no longer makes the app crash</li>
			<li>Options to change start have been added under &quot;Personalization&quot;</li>
			<li>You can no longer pin individual settings to Start</li>
		</ul>
	</li>
	<li>MSN apps
		<ul>
			<li>All MSN apps have been given a small design update</li>
		</ul>
	</li>
	<li>Insider Hub
		<ul>
			<li>The Insider Hub now follows the new design rules for Windows 10 with a brand new interface</li>
		</ul>
	</li>
	<li>3D Builder has been added as a default app</li>
	<li>Windows Reading List is no longer a default app</li>
	<li>Windows Reader is no longer a default app</li>
	<li>Music is no longer a default app</li>
	<li>Video is no longer a default app</li>
</ul>