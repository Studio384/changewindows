<p class="meta build-date">
	<span class="release-version">
		10.04.'15
	</span>
</p>
<h2 class="ctr-warning">Windows 10 "Threshold" &middot; 10061</h2>
<?php echo $alerts; ?>
<h3>Start<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Power button is now showed above the All apps button</li>
	<li>There are some new icons</li>
	<li>It is now possible to resize the start menu</li>
	<li>The amount of collumns per group in the menu might change from 3 to 4 or 4 to 3</li>
	<li>Start now shows the progress of apps that are being installed</li>
	<li>Tiles for Win32 apps no longer take a color based on the icon of that tile, but follows the accent color</li>
	<li>Transparancy can be disabled</li>
	<li>Recently installed apps are now displayed on the top of the all apps list</li>
	<li>Health &amp; Fitness is no longer pinned to start by default</li>
	<li>Cortana can now look for music</li>
</ul>
<h3>"Project Spartan"<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Project Spartan has been updated from version 0.10 to 0.11</li>
	<li>You can now find your downloads in the download manager</li>
	<li>You can now open Internet Explorer from within Spartan</li>
	<li>The &quot;Add to&quot; dialogue has been improved</li>
	<li>You can now save PDFs you opened in the browser</li>
	<li>Spartan now has an about:flags page with new options compared to Internet Explorer</li>
	<li>Edge improvements (also apply to Internet Explorer)
		<ul>
			<li>Major improvements to ECMAScript 6 support (from 73% to 82%)
				<ul>
					<li>Default function parameters</li>
					<li>Rest parameters</li>
					<li>Spread (...) operator</li>
					<li>RegEx &quot;y&quot; and &quot;u&quot; flags</li>
					<li>Generators</li>
					<li>Proxy</li>
					<li>Reflect</li>
					<li>Function &quot;name&quot; property</li>
					<li>String.prototype methods</li>
				</ul>
			</li>
			<li>Improved support for HTML5</li>
			<li>Improved support for CSS3</li>
		</ul>
		<ul>
			<li>JavaScript
				<ul>
					<li>Default parameter</li>
					<li>Generators</li>
					<li>RegExp Build-ins (ES6)</li>
					<li>ASM.js</li>
				</ul>
			</li>
			<li>Multimedia
				<ul>
					<li>Media Capture and Streams</li>
				</ul>
			</li>
			<li>Misc
				<ul>
					<li>&lt;img srcset&gt;</li>
					<li>&lt;main&gt; element</li>
				</ul>
			</li>
			<li>Security
				<ul>
					<li>Meta Referrer</li>
				</ul>
			</li>
			<li>Graphics
				<ul>
					<li>SVG foreignobject element</li>
				</ul>
			</li>
		</ul>
	</li>
</ul>
<h3>Internet Explorer<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The Edge render engine will be removed from Internet Explorer</li>
	<li>IE will be reset back to a state equal to version 11.0.9600</li>
</ul>
<h3>Desktop<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>The taskbar is now transparent</li>
	<li>The TaskView button has a new icon</li>
	<li>The search bar is now the full hight of the taskbar</li>
	<li>Some icons have been updated</li>
	<li>The taskbar is now dark by default</li>
	<li>You can now enable and disable the color of the taskbar and startscreen</li>
	<li>There is a new calendar fly-out for the system tray</li>
	<li>The notification center now takes on the color you've choosen for windows and the taskbar</li>
	<li>Windows no longer take on a color and are now always gray</li>
</ul>
<h3>WinRT<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Apps no longer have a restricted minimum size</li>
</ul>
<h3>Apps<small><a href="http://changewindows.org"> by ChangeWindows.org</a></small></h3>
<ul>
	<li>Settings
		<ul>
			<li>The app has received a minor design update</li>
			<li>Multiple new personalization settings have been added</li>
			<li>New privacy options have been added</li>
			<li>Under &quot;Update &amp; recovery&quot; there is a new &quot;For developers&quot; section, puprose is unknown</li>
		</ul>
	</li>
	<li>Weather
		<ul>
			<li>The Weather tile has a new color</li>
			<li>Weather has received a new UI</li>
		</ul>
	</li>
	<li>Sports
		<ul>
			<li>Sports has received a new UI</li>
		</ul>
	</li>
	<li>Money
		<ul>
			<li>Money has received a new UI</li>
		</ul>
	</li>
	<li>People
		<ul>
			<li>The People app has been updated.</li>
		</ul>
	</li>
	<li>News
		<ul>
			<li>News has received a new UI</li>
			<li>The News app now has a black tile and blue interface</li>
		</ul>
	</li>
	<li>Microsoft Solitaire Collection Preview
		<ul>
			<li>Microsoft Solitaire Collection Preview is now a default app</li>
			<li>The app has, compared to its stable counterpart, a brand new UI</li>
		</ul>
	</li>
	<li>Microsoft Family
		<ul>
			<li>Microsoft Family has been added as an app</li>
		</ul>
	</li>
	<li>Music Preview
		<ul>
			<li>Music Preview has been added as an app</li>
		</ul>
	</li>
	<li>Video Preview
		<ul>
			<li>Video Preview has been added as an app</li>
		</ul>
	</li>
	<li>Outlook
		<ul>
			<li>Mail has been replaced by Outlook</li>
			<li>Outlook now has a ribbon design</li>
		</ul>
	</li>
	<li>Outlook Calendar
		<ul>
			<li>Calendar has been replaced by Outlook Calendar</li>
		</ul>
	</li>
</ul>